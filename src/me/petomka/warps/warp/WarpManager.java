package me.petomka.warps.warp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * Created by Benedikt on 23.07.2017.
 */
public abstract class WarpManager {

    private static WarpManager wm;

    public static WarpManager getWarpManager() {
        return wm;
    }

    public static void setWarpmanager(WarpManager wm) {
        WarpManager.wm = wm;
    }

    public abstract List<Warp> getPlayerWarps();

    public abstract List<Warp> getServerWarps();

    public abstract List<Warp> getPlayersWarps(Player p);

    public abstract Warp getWarp(UUID uuid);

    public abstract void updateWarp(Warp warp);

    public abstract void deleteWarp(Warp warp);

    public static Warp createWarp() {
        return new Warp(new Location(Bukkit.getWorlds().get(0), 0, 0, 0, 0, 0), "NULL", "NULL", 0, null, null, UUID.randomUUID());
    }

    public abstract void setWarp(Warp warp);

}
