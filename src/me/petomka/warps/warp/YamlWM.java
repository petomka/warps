package me.petomka.warps.warp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class YamlWM extends WarpManager {

	private File file;
	private FileConfiguration config;

	public YamlWM() {

		File dir = new File("plugins/Warps/");
		if (!dir.exists()) {
			dir.mkdir();
		}

		File warpsFile = new File("plugins/Warps/warps.yml");
		if (!warpsFile.exists()) {
			try {
				warpsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		file = warpsFile;
		config = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(file);

	}

	private void saveWarpsFile() {
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Warp> getPlayerWarps() {
		return getWarps("player");
	}

	private List<Warp> getWarps(String sectionKey) {
		ArrayList<Warp> result = new ArrayList<>();
		ConfigurationSection section = config.getConfigurationSection(sectionKey);
		if (section == null) {
			return result;
		}
		for (String key : section.getKeys(false)) {
			Warp toAdd = fromUUIDKey(sectionKey, key);
			result.add(toAdd);
		}
		return result.stream().sorted(Comparator.comparing(Warp::getSort_id)).collect(Collectors.toList());
	}

	private Warp fromUUIDKey(String sectionKey, String uuidKey) {
		ConfigurationSection current = config.getConfigurationSection(sectionKey + "." + uuidKey);
		if (current == null) {
			return null;
		}
		return new Warp(
				new Location(
						Bukkit.getWorld(current.getString("world")),
						current.getDouble("x"),
						current.getDouble("y"),
						current.getDouble("z"),
						(float) current.getDouble("yaw"),
						(float) current.getDouble("pitch")
				),
				current.getString("name"),
				current.getString("description"),
				current.getInt("sort_id"),
				current.getItemStack("icon"),
				current.getString("creator") == null ? null : UUID.fromString(current.getString("creator")),
				UUID.fromString(uuidKey)
		);
	}

	@Override
	public List<Warp> getServerWarps() {
		return getWarps("server").stream().sorted(Comparator.comparing(Warp::getSort_id))
				.collect(Collectors.toList());
	}

	@Override
	public List<Warp> getPlayersWarps(Player p) {
		return getPlayerWarps().stream().filter(warp -> warp.getCreator() != null && warp.getCreator()
				.equals(p.getUniqueId())).sorted(Comparator.comparingInt(Warp::getSort_id)).collect(Collectors.toList());
	}

	@Override
	public Warp getWarp(UUID uuid) {
		Warp warp = fromUUIDKey("server", uuid.toString());
		if (warp == null) {
			return fromUUIDKey("player", uuid.toString());
		}
		return warp;
	}

	@Override
	public void updateWarp(Warp warp) {
		setWarp(warp);
	}

	@Override
	public void deleteWarp(Warp warp) {
		String warpOwner = warp.isServerWarp() ? "server." : "player.";
		config.set(warpOwner + warp.getUuid(), null);
		saveWarpsFile();
	}

	@Override
	public void setWarp(Warp warp) {
		String warpOwner = warp.isServerWarp() ? "server." : "player.";
		config.set(warpOwner + warp.getUuid() + ".name", warp.getName());
		config.set(warpOwner + warp.getUuid() + ".description", warp.getDescription());
		config.set(warpOwner + warp.getUuid() + ".creator", warp.isServerWarp() ? null : warp.getCreator().toString());
		config.set(warpOwner + warp.getUuid() + ".icon", warp.getIconRaw());
		config.set(warpOwner + warp.getUuid() + ".sort_id", warp.getSort_id());
		Location loc = warp.getLocation();
		config.set(warpOwner + warp.getUuid() + ".world", loc.getWorld().getName());
		config.set(warpOwner + warp.getUuid() + ".x", loc.getX());
		config.set(warpOwner + warp.getUuid() + ".y", loc.getY());
		config.set(warpOwner + warp.getUuid() + ".z", loc.getZ());
		config.set(warpOwner + warp.getUuid() + ".yaw", loc.getYaw());
		config.set(warpOwner + warp.getUuid() + ".pitch", loc.getPitch());
		saveWarpsFile();
	}
}
