package me.petomka.warps.warp;

import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.itemutil.ItemStackUtils;
import me.petomka.warps.mysql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class MySQLWM extends WarpManager {

    private String tableName = ConfigManager.getStringNP("table-name");

    public MySQLWM() {
        String create = "create table if not exists `" + tableName + "` (`uuid` varchar(50) character set utf8 collate utf8_bin not null, " +
                "`world` varchar(50) character set utf8 collate utf8_bin not null, `x` double not null, `y` double not null, `z` double not null, " +
                "`yaw` float not null, `pitch` float not null, `name` varchar(50) character set utf8 collate utf8_bin not null, " +
                "`description` varchar(256) character set utf8 collate utf8_bin not null, `sort_id` int not null, " +
                "`icon` varchar(1024) character set utf8 collate utf8_bin null, `creator` varchar(50) character set utf8 collate utf8_bin null," +
                "primary key (`uuid`)) engine = MyISAM";
        try {
            MySQL.executeUpdate(create);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public ArrayList<Warp> getPlayerWarps() {
        ArrayList<Warp> result = new ArrayList<>();
        try {
            HashMap<String, ArrayList<Object>> table = MySQL.executeManyQueries("select * from `" + tableName + "` where creator is not null order by sort_id desc");
            for(int i = 0; i < table.get("uuid").size(); i++) {
                World w = Bukkit.getWorld(table.get("world").get(i).toString());
                if(w == null) {
                    continue;
                }
                Location target = new Location(w, (double)table.get("x").get(i), (double)table.get("y").get(i),
                        (double)table.get("z").get(i), (float)table.get("yaw").get(i), (float)table.get("pitch").get(i));
                Object itemStackObject = table.get("icon").get(i);
                if(itemStackObject != null) {
                    itemStackObject = ItemStackUtils.deserialize((String) itemStackObject);
                }
                Warp warp = new Warp(target, table.get("name").get(i).toString(), table.get("description").get(i).toString(),
                        (int)table.get("sort_id").get(i), (ItemStack) itemStackObject, UUID.fromString(table.get("creator").get(i).toString())
                        , UUID.fromString(table.get("uuid").get(i).toString()));
                result.add(warp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ArrayList<Warp> getServerWarps() {
        ArrayList<Warp> result = new ArrayList<>();
        try {
            HashMap<String, ArrayList<Object>> table = MySQL.executeManyQueries("select * from `" + tableName + "` where creator is null order by sort_id desc");
            for(int i = 0; i < table.get("uuid").size(); i++) {
                World w = Bukkit.getWorld(table.get("world").get(i).toString());
                if(w == null) {
                    continue;
                }
                Location target = new Location(w, (double)table.get("x").get(i), (double)table.get("y").get(i),
                        (double)table.get("z").get(i), (float)table.get("yaw").get(i), (float)table.get("pitch").get(i));
                Object itemStackObject = table.get("icon").get(i);
                if(itemStackObject != null) {
                    itemStackObject = ItemStackUtils.deserialize((String) itemStackObject);
                }
                Warp warp = new Warp(target, table.get("name").get(i).toString(), table.get("description").get(i).toString(),
                        (int)table.get("sort_id").get(i), (ItemStack) itemStackObject, null,
                        UUID.fromString(table.get("uuid").get(i).toString()));
                result.add(warp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ArrayList<Warp> getPlayersWarps(Player p) {
        ArrayList<Warp> result = new ArrayList<>();
        try {
            HashMap<String, ArrayList<Object>> table = MySQL.executeManyQueries("select * from `" + tableName + "` " +
                    "where creator = '" + p.getUniqueId().toString() + "' order by sort_id desc");
            for(int i = 0; i < table.get("uuid").size(); i++) {
                World w = Bukkit.getWorld(table.get("world").get(i).toString());
                if(w == null) {
                    continue;
                }
                Location target = new Location(w, (double)table.get("x").get(i), (double)table.get("y").get(i),
                        (double)table.get("z").get(i), (float)table.get("yaw").get(i), (float)table.get("pitch").get(i));
                Object itemStackObject = table.get("icon").get(i);
                if(itemStackObject != null) {
                    itemStackObject = ItemStackUtils.deserialize((String) itemStackObject);
                }
                Warp warp = new Warp(target, table.get("name").get(i).toString(), table.get("description").get(i).toString(),
                        (int)table.get("sort_id").get(i), (ItemStack) itemStackObject, p.getUniqueId(),
                        UUID.fromString(table.get("uuid").get(i).toString()));
                result.add(warp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Warp getWarp(UUID uuid) {
        try {
            HashMap<String, ArrayList<Object>> table = MySQL.executeManyQueries("select * from `" + tableName + "` where uuid = '" + uuid + "'");
            if(table == null) {
                return null;
            }
            World w = Bukkit.getWorld(table.get("world").get(0).toString());
            if(w == null) {
                return null;
            }
            Location target = new Location(w, (double)table.get("x").get(0), (double)table.get("y").get(0),
                    (double)table.get("z").get(0), (float)table.get("yaw").get(0), (float)table.get("pitch").get(0));
            Object itemStackObject = table.get("icon").get(0);
            if(itemStackObject != null) {
                itemStackObject = ItemStackUtils.deserialize((String) itemStackObject);
            }
            return new Warp(target, table.get("name").get(0).toString(), table.get("description").get(0).toString(),
                    (int)table.get("sort_id").get(0), (ItemStack) itemStackObject, table.get("creator").get(0) == null ? null : UUID.fromString(table.get("creator").get(0).toString()),
                    UUID.fromString(table.get("uuid").get(0).toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateWarp(Warp warp) {
        Location loc = warp.getLocation();
        try {
            MySQL.executeUpdate("update `" + tableName + "` set world = '" + loc.getWorld().getName() + "', x = " +
                    + loc.getX() + " , y = " + loc.getY() + " , z = " + loc.getZ() + " , yaw = " + loc.getYaw() +
                    " , pitch = " + loc.getPitch() + ", name = '" + warp.getName() + "' , description = " +
                    "'" + warp.getDescription() + "' , sort_id = " + warp.getSort_id() + " , icon = '" + warp.getSerializedItem() + "' , creator = " +
                    (warp.isServerWarp() ? "NULL" : "'" + warp.getCreator().toString() + "'")
                    + " where uuid = '" + warp.getUuid().toString() + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteWarp(Warp warp) {
        try {
            MySQL.executeUpdate("delete from `" + tableName + "` where uuid = '" + warp.getUuid().toString() + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setWarp(Warp warp) {
        Location loc = warp.getLocation();
        try {
            PreparedStatement st = MySQL.registerParameters("insert into `" + tableName + "` (`uuid`, `world`, `x`, `y`, `z`, `yaw`, `pitch`, " +
                    "`name`, `description`, `sort_id`, `icon`, `creator`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    warp.getUuid().toString(), loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch(), warp.getName(),
                    warp.getDescription(), warp.getSort_id(), warp.getSerializedItem(),
                    warp.isServerWarp() ? null : warp.getCreator().toString());
            MySQL.executeAndUpdate(st);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
