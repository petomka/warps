package me.petomka.warps.warp;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.itemutil.ItemStackUtils;
import me.petomka.warps.uuidfetcher.UUIDFetcher;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class Warp {

	private Location location;
	private String name;
	private String description;
	private int sort_id;
	private ItemStack icon;
	private UUID creator;
	private final UUID uuid;

	Warp(@Nonnull Location loc, @Nonnull String name, @Nullable String desc, int sort_id, @Nullable ItemStack icon, @Nullable UUID creator, UUID uuid) {
		this.location = loc;
		this.name = ChatColor.translateAlternateColorCodes('&', name);
		this.description = desc;
		this.sort_id = sort_id;
		this.icon = icon;
		this.creator = creator;
		this.uuid = uuid;
		if (this.icon == null || this.icon.getType() == Material.AIR) {
			this.icon = getIcon();
		}
		ItemMeta iconMeta = this.icon.getItemMeta();
		setName(name);
		iconMeta.setDisplayName("§r" + getName());
		iconMeta.setLore(loreFromDescription(desc));
		this.icon.setItemMeta(iconMeta);
	}

	private List<String> loreFromDescription(@Nullable String desc) {
		if (desc == null) {
			return Collections.emptyList();
		}
		String[] lines = desc.replace("\n", "\\n").split("\\\\n");
		ArrayList<String> result = new ArrayList<>();
		List<String> configLore = Main.config.getStringList("menu.icon.description");
		for (String s : configLore) {
			s = s.replaceAll("&", "§").replaceAll("§§", "&");

			s = s.replaceAll("<creator>", getCreator() == null ? ConfigManager.getStringNP("server-name") : UUIDFetcher.getName(getCreator()));
			if (s.contains("<description>")) {
				for (int a = 0; a < lines.length; a++) {
					if (a == 0) {
						s = s.replaceAll("<description>", lines[a].replaceAll("&", "§").replaceAll("§§", "&"));
						result.add(s);
						continue;
					}
					result.add(lines[a].replaceAll("&", "§").replaceAll("§§", "&"));
				}
				continue;
			}
			result.add(s);
		}
		return result;
	}

	public Warp(Location loc, String name, UUID uuid) {
		this(loc, name, uuid, null);
	}

	public Warp(Location loc, String name, UUID uuid, UUID creator) {
		this(loc, name, "NULL", 0, null, creator, uuid);
	}

	public final Location getLocation() {
		return location;
	}

	public final String getName() {
		if (name == null) {
			return "§rNULL";
		}
		return ChatColor.translateAlternateColorCodes('&', name);
	}

	public final String getDescription() {
		return description;
	}

	public final int getSort_id() {
		return sort_id;
	}

	public final @Nullable
	UUID getCreator() {
		return creator;
	}

	public final boolean isServerWarp() {
		return creator == null;
	}

	public final @Nonnull
	ItemStack getIcon() {
		if (icon == null || this.icon.getType() == Material.AIR) {
			return new ItemStack(Material.COMPASS);
		}
		return icon;
	}

	public final @Nonnull
	ItemStack getIconRaw() {
		ItemStack is = new ItemStack(getIcon().getType());
		ItemMeta meta = getIcon().getItemMeta();
		meta.setLore(new ArrayList<>());
		meta.setDisplayName("");
		is.setItemMeta(meta);
		return is;
	}

	public String getSerializedItem() {
		return ItemStackUtils.serialize(getIcon());
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setLocation(@Nonnull Location location) {
		this.location = location;
	}

	public void setName(@Nonnull String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setSort_id(int sort_id) {
		this.sort_id = sort_id;
	}

	public void setIcon(@Nullable ItemStack icon) {
		this.icon = icon;
	}

	public void setCreator(@Nullable UUID creator) {
		this.creator = creator;
	}

	public void updateInformationStorage() {
		WarpManager.getWarpManager().updateWarp(this);
	}
}
