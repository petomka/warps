package me.petomka.warps.commands;

import me.petomka.warps.config.ConfigManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class CommandManager {

    private static ArrayList<Command> commands = new ArrayList<>();

    public static void registerCommand(Command cmd) {
        commands.add(cmd);
    }

    public static void onCommand(CommandSender sender, org.bukkit.command.Command bukkitCommand, String label, String[] args) {
        for(Command cmd : commands) {
            if(cmd.getName().equalsIgnoreCase(bukkitCommand.getName())) {
                if(sender.hasPermission(cmd.getPermission())) {
                    if(!cmd.execute(sender, label, args)) {
                        sender.sendMessage(ConfigManager.getPrefix() + cmd.getUsage());
                    }
                } else {
                    sender.sendMessage(ConfigManager.getString("nopermission"));
                }
            }
        }
    }

}
