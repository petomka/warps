package me.petomka.warps.commands;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.menu.MenuManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class CommandWarp extends Command {

    public CommandWarp() {
        super("warp", "/warp [Warpname]", "warps.warp");
    }

    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage("This command has to be issued from in-game.");
            return true;
        }
        if(args.length > 0) {
            if(args[0].equalsIgnoreCase("reload")) {
                if(!sender.hasPermission("warps.admin")) {
                    sender.sendMessage(ConfigManager.getString("nopermission"));
                    return true;
                }
                Main.main.reloadConfig_();
                sender.sendMessage(ConfigManager.getString("config-reloaded"));
                return true;
            }
        }
        MenuManager.openMainMenu((Player)sender);
        return true;
    }

}
