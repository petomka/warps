package me.petomka.warps.commands;

import org.bukkit.command.CommandSender;

/**
 * Created by Benedikt on 23.07.2017.
 */
public abstract class Command {

    private final String name;
    private final String usage;
    private final String permission;

    public Command(String name, String usage, String permission) {
        this.name = name;
        this.usage = usage;
        this.permission = permission;
        CommandManager.registerCommand(this);
    }

    public abstract boolean execute(CommandSender sender, String label, String[] args);

    public String getName() {
        return name;
    }

    public String getUsage() {
        return usage;
    }

    public String getPermission() {
        return permission;
    }
}
