package me.petomka.warps.teleports;

import me.petomka.warps.Main;
import me.petomka.warps.bossbar.BossBar;
import me.petomka.warps.config.ConfigManager;
import net.minecraft.server.v1_12_R1.BossBattle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;

/**
 * Created by Benedikt on 24.07.2017.
 */
public class TeleportTask implements Listener {

    private boolean cancelled = false;

    private int seconds;
    private Player player;
    private Location location;
    private BossBar bar;

    private static HashMap<Player, TeleportTask> teleporting = new HashMap<>();

    public static void queueTeleport(Player p, Location loc) {
        if (teleporting.containsKey(p)) {
            teleporting.get(p).cancelInstant();
        }
        if (loc == null) {
            throw new IllegalArgumentException("Cannot teleport to null");
        }
        teleporting.put(p, new TeleportTask(p, loc));
    }

    private TeleportTask(Player player, Location loc) {
        seconds = Main.config.getInt("teleport-delay-seconds");
        this.player = player;
        this.location = loc;
        if (player.hasPermission("warps.bypass")) {
            teleport();
            return;
        }
        bar = new BossBar(ConfigManager.getStringNP("teleport-bar-message").replaceAll("<seconds>", String.valueOf(seconds)));
        bar.setBarColor(BossBattle.BarColor.GREEN);
        bar.setBarStyle(BossBattle.BarStyle.NOTCHED_10);
        bar.addToPlayer(player);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, new RemoveBarTask(), (seconds*20L) + 20L);
        for (int i = seconds; i >= 0; i--) {
            if (i == 0) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, new TeleportPlayerTask(), seconds * 20);
            }
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, new UpdateBarTask(i), (seconds - i) * 20);
        }
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void cancelInstant() {
        if (cancelled) {
            return;
        }
        HandlerList.unregisterAll(this);
        cancelled = true;
        bar.removeFromPlayer(player);
    }

    private void teleport() {
        player.teleport(location);
        HandlerList.unregisterAll(this);
        location = null;
        cancelled = true;
    }

    private class UpdateBarTask implements Runnable {

        private int second;

        public UpdateBarTask(int second) {
            this.second = second;
        }

        public void run() {
            if (cancelled) {
                return;
            }
            bar.updateHealth((float) second / (float) seconds);
            bar.updateText(ConfigManager.getStringNP("teleport-bar-message").replaceAll("<seconds>", String.valueOf(second)));
        }

    }

    private class TeleportPlayerTask implements Runnable {

        public void run() {
            if(cancelled) {
                return;
            }
            bar.updateHealth(0);
            bar.updateText(ConfigManager.getStringNP("teleport-bar-message").replaceAll("<seconds>", "0"));
            teleport();
        }

    }

    /**
     * Because for some weird reason calling Bukkit scheduler from a running synchron task didn't work
     */
    private class RemoveBarTask implements Runnable {

        public void run() {
            if(bar == null || player == null) {
                return;
            }
            bar.removeFromPlayer(player);
            player = null;
            bar = null;
        }

    }

    private void cancelTP() {
        cancelled = true;
        bar.updateHealth(1f);
        bar.updateStyle(BossBattle.BarStyle.PROGRESS, BossBattle.BarColor.RED);
        bar.updateText(ConfigManager.getStringNP("teleport-bar-message-interrupt"));
        Bukkit.getScheduler().runTaskLater(Main.main, () -> {
            bar.removeFromPlayer(player);
            this.bar = null;
            this.player = null;
        }, 60L);
        location = null;
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (!e.getPlayer().equals(player)) {
            return;
        }
        Location from = e.getFrom();
        Location to = e.getTo();
        if(from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() || from.getBlockZ() != to.getBlockZ()) {
            cancelTP();
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if (!e.getEntity().equals(player)) {
            return;
        }
        cancelTP();
    }

}
