package me.petomka.warps;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import me.petomka.warps.commands.CommandManager;
import me.petomka.warps.commands.CommandWarp;
import me.petomka.warps.mysql.MySQL;
import me.petomka.warps.warp.MySQLWM;
import me.petomka.warps.warp.WarpManager;
import me.petomka.warps.warp.YamlWM;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.logging.Level;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class Main extends JavaPlugin {

    public static Main main;
    public static FileConfiguration config;
    public static ProtocolManager protocolManager;
    public static Economy economy;

    @Override
    public void onDisable() {
        getLogger().info("Warps disabled");
    }

    @Override
    public void onEnable() {
        Main.main = this;
        getLogger().info("Warps enabled");
        protocolManager = ProtocolLibrary.getProtocolManager();
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        economy = rsp.getProvider();
        initConfig();
        initCommands();
        if(config.getBoolean("use-mysql")) {
            try {
                MySQL.initMySQL();
                WarpManager.setWarpmanager(new MySQLWM());
            } catch (Exception e) {
                getLogger().log(Level.SEVERE, "Warps could not connect to MySQL database. Please setup the connection properly or disable MySQL in your config.yml. " +
                        "Disabling Warps!");
                Bukkit.getPluginManager().disablePlugin(this);
            }
        } else {
            WarpManager.setWarpmanager(new YamlWM());
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        CommandManager.onCommand(sender, command, label, args);
        return true;
    }

    public void reloadConfig_() {
        reloadConfig();
        Main.config = this.getConfig();
    }

    private void initConfig() {
        Main.config = this.getConfig();

        config.addDefault("prefix", "&5&lWarp &7»&r ");
        config.addDefault("nopermission", "&cYou do not have permission.");

        config.addDefault("server-name", "Server");
        config.addDefault("table-name", "warps_warps");

        config.addDefault("use-mysql", false);

        config.addDefault("config-reloaded", "&aConfiguration was reloaded.");

        config.addDefault("enable-player-warps", true);
        config.addDefault("grow-price", true);
        config.addDefault("warp-price", 50_000.0d);
        config.addDefault("better-sort-price", 8_000d);

        config.addDefault("teleport-delay-seconds", 5);
        config.addDefault("teleport-bar-message", "&aTeleport in &e<seconds> &asecond(s)");
        config.addDefault("teleport-bar-message-interrupt", "&c&lTeleportation interrupted!");

        config.addDefault("menu.option.player-warps", "&2&l&nPlayer Warps");
        config.addDefault("menu.option.edit-own-warps", "&3&l&nYour Warps");
        config.addDefault("menu.option.server-warps", "&c&l&nServer Warps");

        config.addDefault("menu.option.add-own-warp", Arrays.asList("&3&lBuy a new Warp", "&7Price: &e<price> <symbol>"));
        config.addDefault("menu.option.add-server-warp", "&c&lAdd Sever Warp");
        config.addDefault("menu.option.page-up", "&eNext page");
        config.addDefault("menu.option.page-down", "&bPrevious page");
        config.addDefault("menu.option.back-to-menu", "&cBack to main menu");
        config.addDefault("menu.option.right-click-edit", "&cRight click to edit");

        config.addDefault("menu.option.edit.info", Arrays.asList("&1&lInfo", "&7UUID: <uuid>", "&7Creator: <creator>", "&7Sorting: <sort>"));
        config.addDefault("menu.option.edit.location", "&9&lEdit Warp location");
        config.addDefault("menu.option.edit.name", "&a&lChange name");
        config.addDefault("menu.option.edit.description", "&2&lChange description");
        config.addDefault("menu.option.edit.icon", "&e&lChange icon");
        config.addDefault("menu.option.edit.sort.buy", Arrays.asList("&6&lBuy better sorting", "&7Price: &e<price> <symbol>"));
        config.addDefault("menu.option.edit.sort.admin", "&6&lEdit sorting");
        config.addDefault("menu.option.edit.delete", "&c&lDelete Warp");

        config.addDefault("menu.title.main-menu", "&5&lWarps");
        config.addDefault("menu.title.player-warps", "&2&lPlayer Warps");
        config.addDefault("menu.title.server-warps", "&c&lServer Warps");
        config.addDefault("menu.title.own-warps", "&3&lYour Warps");
        config.addDefault("menu.title.edit-warp", "&6&lEdit Warp");

        config.addDefault("menu.icon.description", Arrays.asList("&8&m*-------------------------*", "&7Creator: <creator>",
                "&7Description: <description>", "&8&m*-------------------------*"));

        config.addDefault("config.quit-config", "&eConfirmed.");
        config.addDefault("config.quit-string", "done");
        config.addDefault("config.quit-info", "&aTo finish the configurator, type \"&e<quit>&a\".");
        config.addDefault("config.enter-string", "&ePlease enter a String. &7(<property>)");
        config.addDefault("config.property.name", "Name");
        config.addDefault("config.property.description", "Description");
        config.addDefault("config.enter-int", "&ePlease enter an integer.");
        config.addDefault("config.sneak-position", "&aTo set a position for your Warp, please &csneak &awhere you would like your location to be");
        config.addDefault("config.click-item", "&aTo set an icon for your Warp, please &cright click &abwith an Item!");
        config.addDefault("config.success", "&aProperty successfully changed.");

        config.addDefault("setup.success", "&aNew Warp was successfully set.");
        config.addDefault("setup.deleted", "&7Warp has been &cdeleted&7.");

        config.addDefault("menu.no-page", "&cThis page does not exist!");
        config.addDefault("menu.no-funds-warp", "&7You &ccannot afford&7 to buy a new Warp!");
        config.addDefault("menu.no-funds-sort", "&7You &cncannot afford&7, to buy a better sorting for your Warp!");

        config.options().copyDefaults(true);
        this.saveConfig();
    }

    private void initCommands() {
        new CommandWarp();
    }
}
