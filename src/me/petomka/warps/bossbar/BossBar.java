package me.petomka.warps.bossbar;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import me.petomka.warps.Main;
import net.minecraft.server.v1_12_R1.BossBattle;
import net.minecraft.server.v1_12_R1.PacketPlayOutBoss;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Benedikt on 10.07.2017.
 */
public class BossBar {

    private UUID uuid;

    private BossBattle.BarColor barColor;
    private BossBattle.BarStyle barStyle;
    private String text;
    private float health;

    private boolean darkenSky;
    private boolean playMusic;
    private boolean createFog;

    private ArrayList<Player> receivingPlayers = new ArrayList<>();

    public BossBar(String text, float value, BossBattle.BarColor color, BossBattle.BarStyle style, boolean darkenSky, boolean playMusic, boolean createFog) {
        this.text = text;
        this.barColor = color;
        this.barStyle = style;
        this.darkenSky = darkenSky;
        this.playMusic = playMusic;
        this.createFog = createFog;
        this.uuid = UUID.randomUUID();
        this.health = value;
    }

    public BossBar(String text, float value, BossBattle.BarColor color, BossBattle.BarStyle style) {
        this(text, value, color, style, false, false, false);
    }

    public BossBar(String text, float value) {
        this(text, value, BossBattle.BarColor.PURPLE, BossBattle.BarStyle.PROGRESS);
    }

    public BossBar(String text) {
        this(text, 1.0f);
    }

    public void addToPlayer(Player player) {
        if (receivingPlayers.contains(player)) {
            return;
        }
        if (sendAdd(player)) {
            receivingPlayers.add(player);
        }
    }

    private boolean sendAdd(Player player) {
        PacketContainer pc = Main.protocolManager.createPacket(PacketType.Play.Server.BOSS);
        pc.getUUIDs().write(0, uuid);
        pc.getEnumModifier(PacketPlayOutBoss.Action.class, 1).write(0, PacketPlayOutBoss.Action.ADD);
        pc.getChatComponents().write(0, WrappedChatComponent.fromText(this.text));
        pc.getFloat().write(0, health);
        pc.getEnumModifier(BossBattle.BarColor.class, 4).write(0, barColor);
        pc.getEnumModifier(BossBattle.BarStyle.class, 5).write(0, barStyle);
        pc.getBooleans().write(0, darkenSky).write(1, playMusic).write(2, createFog);
        try {
            Main.protocolManager.sendServerPacket(player, pc);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void removeFromPlayer(Player player) {
        if (!receivingPlayers.contains(player)) {
            return;
        }
        if (sendRemove(player)) {
            receivingPlayers.remove(player);
        }
    }

    private boolean sendRemove(Player player) {
        PacketContainer pc = Main.protocolManager.createPacket(PacketType.Play.Server.BOSS);
        pc.getUUIDs().write(0, uuid);
        pc.getEnumModifier(PacketPlayOutBoss.Action.class, 1).write(0, PacketPlayOutBoss.Action.REMOVE);
        try {
            Main.protocolManager.sendServerPacket(player, pc);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Sends the update for health to every receiving player
     *
     * @param health float value in percent (0-1)
     */
    public void updateHealth(float health) {
        this.health = health;
        PacketContainer pc = Main.protocolManager.createPacket(PacketType.Play.Server.BOSS);
        pc.getUUIDs().write(0, uuid);
        pc.getEnumModifier(PacketPlayOutBoss.Action.class, 1).write(0, PacketPlayOutBoss.Action.UPDATE_PCT);
        pc.getFloat().write(0, health);
        sendUpdate(pc);
    }

    /**
     * Sends the update with new text to every added player
     *
     * @param text the new text value
     */
    public void updateText(String text) {
        PacketContainer pc = Main.protocolManager.createPacket(PacketType.Play.Server.BOSS);
        pc.getUUIDs().write(0, uuid);
        pc.getEnumModifier(PacketPlayOutBoss.Action.class, 1).write(0, PacketPlayOutBoss.Action.UPDATE_NAME);
        pc.getChatComponents().write(0, WrappedChatComponent.fromText(text));
        sendUpdate(pc);
    }

    /**
     * Updates the style and sends the update to every added player
     *
     * @param style BarStyle
     * @param color BarColor
     */
    public void updateStyle(BossBattle.BarStyle style, BossBattle.BarColor color) {
        PacketContainer pc = Main.protocolManager.createPacket(PacketType.Play.Server.BOSS);
        pc.getUUIDs().write(0, uuid);
        pc.getEnumModifier(PacketPlayOutBoss.Action.class, 1).write(0, PacketPlayOutBoss.Action.UPDATE_STYLE);
        pc.getEnumModifier(BossBattle.BarColor.class, 4).write(0, color);
        pc.getEnumModifier(BossBattle.BarStyle.class, 5).write(0, style);
        sendUpdate(pc);
    }

    public void updateProperties(boolean darkenSky, boolean playMusic, boolean createFog) {
        PacketContainer pc = Main.protocolManager.createPacket(PacketType.Play.Server.BOSS);
        pc.getUUIDs().write(0, uuid);
        pc.getEnumModifier(PacketPlayOutBoss.Action.class, 1).write(0, PacketPlayOutBoss.Action.UPDATE_PROPERTIES);
        pc.getBooleans().write(0, darkenSky).write(1, playMusic).write(2, createFog);
        sendUpdate(pc);
    }

    private void sendUpdate(PacketContainer pc) {
        try {
            for (Player player : receivingPlayers) {
                Main.protocolManager.sendServerPacket(player, pc);
            }
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void resendForEveryAddedPlayer() {
        for(Player p : receivingPlayers) {
            sendRemove(p);
            sendAdd(p);
        }
    }

    public BossBattle.BarColor getBarColor() {
        return barColor;
    }

    public void setBarColor(BossBattle.BarColor barColor) {
        this.barColor = barColor;
    }

    public BossBattle.BarStyle getBarStyle() {
        return barStyle;
    }

    public void setBarStyle(BossBattle.BarStyle barStyle) {
        this.barStyle = barStyle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public boolean isDarkenSky() {
        return darkenSky;
    }

    public void setDarkenSky(boolean darkenSky) {
        this.darkenSky = darkenSky;
    }

    public boolean isPlayMusic() {
        return playMusic;
    }

    public void setPlayMusic(boolean playMusic) {
        this.playMusic = playMusic;
    }

    public boolean isCreateFog() {
        return createFog;
    }

    public void setCreateFog(boolean createFog) {
        this.createFog = createFog;
    }

    public ArrayList<Player> getReceivingPlayers() {
        return receivingPlayers;
    }
}
