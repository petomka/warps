package me.petomka.warps.menu;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Benedikt on 27.07.2017.
 */
public class ConfigureFinishEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private Player player;

    public ConfigureFinishEvent(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
