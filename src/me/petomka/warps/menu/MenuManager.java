package me.petomka.warps.menu;

import me.petomka.warps.config.ConfigManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class MenuManager {

    public static void openMainMenu(Player p) {
        if(ChatConfigurator.isEditing(p)) {
            return;
        }
        new MainMenu(p);
    }

    public static ItemStack[] getPagesSelectors() {
        ItemStack mainMenu = new ItemStack(Material.BARRIER);
        ItemMeta mmm = mainMenu.getItemMeta();
        mmm.setDisplayName(ConfigManager.getStringNP("menu.option.back-to-menu"));
        mainMenu.setItemMeta(mmm);

        ItemStack pageDown = new ItemStack(Material.GHAST_TEAR);
        ItemMeta pdm = pageDown.getItemMeta();
        pdm.setDisplayName(ConfigManager.getStringNP("menu.option.page-down"));
        pageDown.setItemMeta(pdm);

        ItemStack pageUp = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta pum = pageUp.getItemMeta();
        pum.setDisplayName(ConfigManager.getStringNP("menu.option.page-up"));
        pageUp.setItemMeta(pum);

        return new ItemStack[]{mainMenu, pageDown, pageUp};
    }

}
