package me.petomka.warps.menu;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.teleports.TeleportTask;
import me.petomka.warps.warp.Warp;
import me.petomka.warps.warp.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class MenuServerWarps implements Menu, Listener {

    private Inventory inv = null;
    private int page = 0, mainMenu = 42, pageDown = 43, pageUp = 44, createServerWarp = 40;

    private List<Warp> warps;

    public MenuServerWarps(Player player) {
        player.closeInventory();
        createInventory(player);
        player.openInventory(inv);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory(Player player) {
        if (inv == null) {
            inv = Bukkit.createInventory(player, 9 * 5, ConfigManager.getStringNP("menu.title.server-warps"));
        }

        ItemStack black = new ItemStack(Material.STAINED_GLASS_PANE);
        black.setDurability((short) 15);
        ItemMeta meta = black.getItemMeta();
        meta.setDisplayName("§r");
        black.setItemMeta(meta);

        ItemStack blue = new ItemStack(Material.STAINED_GLASS_PANE);
        blue.setDurability((short) 11);
        blue.setItemMeta(meta);

        warps = WarpManager.getWarpManager().getServerWarps();

        for (int index = 0; index < 9 * 5; index++) {
            if (index > 35) {
                inv.setItem(index, blue);
            } else {
                if (index + ((page * 36)) < warps.size()) {
                    ItemStack icon = warps.get(index + (page*36)).getIcon();
                    if(player.hasPermission("warps.admin")) {
                        ItemMeta iconMeta = icon.getItemMeta();
                        List<String> lorelist = iconMeta.getLore();
                        lorelist.add(ConfigManager.getStringNP("menu.option.right-click-edit"));
                        iconMeta.setLore(lorelist);
                        icon.setItemMeta(iconMeta);
                    }
                    inv.setItem(index, icon);
                } else {
                    inv.setItem(index, black);
                }
            }
        }

        ItemStack[] ps = MenuManager.getPagesSelectors();

        if (Main.config.getBoolean("enable-player-warps")) {
            inv.setItem(mainMenu, ps[0]);
        }
        inv.setItem(pageDown, ps[1]);
        inv.setItem(pageUp, ps[2]);

        if (!player.hasPermission("warps.admin")) {
            return;
        }

        ItemStack createWarp = new ItemStack(Material.ANVIL);
        ItemMeta cwm = createWarp.getItemMeta();
        cwm.setDisplayName(ConfigManager.getStringNP("menu.option.add-server-warp"));
        createWarp.setItemMeta(cwm);

        inv.setItem(createServerWarp, createWarp);

    }

    private boolean disposed = false;

    private void dispose() {
        dispose(false);
    }

    private void dispose(boolean close) {
        if (disposed) {
            return;
        }
        disposed = true;
        HandlerList.unregisterAll(this);
        if(close) {
            ((Player) inv.getHolder()).closeInventory();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (!e.getInventory().equals(inv)) {
            return;
        }
        dispose();
        if (Main.config.getBoolean("enable-player-warps")) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, () -> new MainMenu((Player) inv.getHolder()), 1L);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (!e.getInventory().equals(inv)) {
            return;
        }

        e.setCancelled(true);

        if (e.getClick() == ClickType.LEFT) {
            if (e.getRawSlot() < 36) {
                if (e.getRawSlot() + page * 36 >= warps.size()) {
                    return;
                }
                int index = e.getRawSlot() + page * 36;
                if (index < 0) {
                    return;
                }
                dispose(true);
                TeleportTask.queueTeleport((Player) inv.getHolder(), warps.get(index).getLocation());
            }

            if (e.getRawSlot() == mainMenu) {
                dispose(true);
                if (Main.config.getBoolean("enable-player-warps")) {
                    new MainMenu((Player) inv.getHolder());
                }
            } else if (e.getRawSlot() == pageDown) {
                if (page < 1) {
                    ((Player) inv.getHolder()).sendMessage(ConfigManager.getString("menu.no-page"));
                    return;
                }
                page -= 1;
                createInventory((Player) inv.getHolder());
            } else if (e.getRawSlot() == pageUp) {
                if (!((page + 1) * 9 * 4 < warps.size())) {
                    ((Player) inv.getHolder()).sendMessage(ConfigManager.getString("menu.no-page"));
                    return;
                }
                page += 1;
                createInventory((Player) inv.getHolder());
            } else if (e.getRawSlot() == createServerWarp) {
                if (!((Player) inv.getHolder()).hasPermission("warps.admin")) {
                    return;
                }
                dispose(true);
                new ChatSetup((Player) e.getWhoClicked(), false);
            }
            return;
        } else if (e.getClick() == ClickType.RIGHT) {
            if (!e.getWhoClicked().hasPermission("warps.admin")) {
                return;
            }
            int index = e.getRawSlot() + page * 36;
            dispose(true);
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, () -> new MenuWarpEdit((Player)e.getWhoClicked(), warps.get(index), true), 1L);
        }

    }

}
