package me.petomka.warps.menu;

/**
 * Created by Benedikt on 23.07.2017.
 */
public enum Property {

    NAME,
    DESCRIPTION,
    LOCATION,
    SORT_ID,
    ICON;

}
