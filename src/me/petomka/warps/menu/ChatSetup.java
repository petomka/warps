package me.petomka.warps.menu;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.warp.Warp;
import me.petomka.warps.warp.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 * Created by Benedikt on 27.07.2017.
 */
public class ChatSetup implements Listener {

    private Player player;

    private boolean isPlayerWarp;

    private Warp newWarp;

    private int configurationStep = 0;

    public ChatSetup(Player player, boolean isPlayerWarp) {
        this.player = player;
        this.isPlayerWarp = isPlayerWarp;
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        newWarp = WarpManager.createWarp();
        if(isPlayerWarp) {
            newWarp.setCreator(player.getUniqueId());
        }
        nextConfiguration();
    }

    private void nextConfiguration() {
        switch (configurationStep) {
            case 0: {
                ChatConfigurator.newEditor(player, Property.NAME, newWarp);
                break;
            }
            case 1: {
                ChatConfigurator.newEditor(player, Property.DESCRIPTION, newWarp);
                break;
            }
            case 2: {
                ChatConfigurator.newEditor(player, Property.ICON, newWarp);
                break;
            }
            case 3: {
                ChatConfigurator.newEditor(player, Property.LOCATION, newWarp);
                break;
            }
            case 4: {
                if (!isPlayerWarp) {
                    ChatConfigurator.newEditor(player, Property.SORT_ID, newWarp);
                } else {
                    doneConfiguration();
                }
                break;
            }
            case 5: {
                doneConfiguration();
            }
        }
        ++configurationStep;
    }

    private void doneConfiguration() {
        HandlerList.unregisterAll(this);
        if(isPlayerWarp) {
            newWarp.setCreator(player.getUniqueId());
            newWarp.setSort_id(1);
        }
        WarpManager.getWarpManager().setWarp(newWarp);
        player.sendMessage(ConfigManager.getString("setup.success"));
        player = null;
    }

    @EventHandler
    public void onConfigureFinish(ConfigureFinishEvent event) {
        if (!event.getPlayer().equals(player)) {
            return;
        }
        nextConfiguration();
    }

}
