package me.petomka.warps.menu;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.uuidfetcher.UUIDFetcher;
import me.petomka.warps.warp.Warp;
import me.petomka.warps.warp.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benedikt on 29.07.2017.
 */
public class MenuWarpEdit implements Menu, Listener {

    private Player player;
    private Warp warp;
    private Inventory inv;
    private boolean isAdmin;

    private int slotInfo = 1, slotLoc = 2, slotName = 3, slotDesc = 4, slotIcon = 5, slotSort = 6, slotDelete = 7;

    public MenuWarpEdit(Player player, Warp w, boolean admin) {
        this.player = player;
        this.warp = w;
        this.isAdmin = admin;
        createInventory();
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        player.openInventory(inv);
    }

    private void createInventory() {
        inv = Bukkit.createInventory(player, 9, ConfigManager.getStringNP("menu.title.edit-warp"));

        ItemStack info = new ItemStack(Material.BEDROCK);
        {
            ItemMeta infoMeta = info.getItemMeta();
            String[] nameAndLore = ConfigManager.getStringList("menu.option.edit.info").toArray(new String[0]);
            if (nameAndLore.length == 0) {
                throw new IllegalArgumentException("List in config at menu.option.edit.info cannot be empty!");
            }
            infoMeta.setDisplayName(nameAndLore[0].replaceAll("<uuid>", warp.getUuid().toString())
                    .replaceAll("<creator>", warp.isServerWarp() ? ConfigManager.getStringNP("server-name") : UUIDFetcher.getName(warp.getCreator())));
            List<String> lore = new ArrayList<>();
            for (int a = 1; a < nameAndLore.length; a++) {
                lore.add(nameAndLore[a].replaceAll("<uuid>", warp.getUuid().toString())
                        .replaceAll("<creator>", warp.isServerWarp() ? ConfigManager.getStringNP("server-name") : UUIDFetcher.getName(warp.getCreator())).replaceAll("<sort>", String.valueOf(warp.getSort_id())));
            }
            infoMeta.setLore(lore);
            info.setItemMeta(infoMeta);
        }

        ItemStack editLoc = new ItemStack(Material.COMPASS);
        ItemMeta editLocMeta = editLoc.getItemMeta();
        editLocMeta.setDisplayName(ConfigManager.getStringNP("menu.option.edit.location"));
        editLoc.setItemMeta(editLocMeta);

        ItemStack editName = new ItemStack(Material.SIGN);
        ItemMeta editNameMeta = editName.getItemMeta();
        editNameMeta.setDisplayName(ConfigManager.getStringNP("menu.option.edit.name"));
        editName.setItemMeta(editNameMeta);

        ItemStack editDesc = new ItemStack(Material.BOOK);
        ItemMeta editDescMeta = editDesc.getItemMeta();
        editDescMeta.setDisplayName(ConfigManager.getStringNP("menu.option.edit.description"));
        editDesc.setItemMeta(editDescMeta);

        ItemStack editIcon = new ItemStack(Material.ITEM_FRAME);
        ItemMeta editIconMeta = editIcon.getItemMeta();
        editIconMeta.setDisplayName(ConfigManager.getStringNP("menu.option.edit.icon"));
        editIcon.setItemMeta(editIconMeta);

        ItemStack editSort = new ItemStack(Material.FEATHER);
        ItemMeta editSortMeta = editSort.getItemMeta();
        if(this.isAdmin) {
            editSortMeta.setDisplayName(ConfigManager.getStringNP("menu.option.edit.sort.admin"));
        } else {
            String[] nameAndLore = ConfigManager.getStringList("menu.option.edit.sort.buy").toArray(new String[0]);
            double price = Main.config.getDouble("better-sort-price");
            editSortMeta.setDisplayName(nameAndLore[0].replaceAll("<price>", String.valueOf(price)));
            List<String> lore = new ArrayList<>();
            for(int a = 1; a < nameAndLore.length; a++) {
                lore.add(nameAndLore[a].replaceAll("<price>", String.valueOf(price)));
            }
            editSortMeta.setLore(lore);
        }
        editSort.setItemMeta(editSortMeta);

        ItemStack delete = new ItemStack(Material.LAVA_BUCKET);
        ItemMeta deleteMeta = delete.getItemMeta();
        deleteMeta.setDisplayName(ConfigManager.getStringNP("menu.option.edit.delete"));
        delete.setItemMeta(deleteMeta);

        inv.setItem(slotInfo, info);
        inv.setItem(slotLoc, editLoc);
        inv.setItem(slotName, editName);
        inv.setItem(slotDesc, editDesc);
        inv.setItem(slotIcon, editIcon);
        inv.setItem(slotSort, editSort);
        inv.setItem(slotDelete, delete);

    }

    private boolean disposed = false;

    private void dispose() {
        dispose(false);
    }

    private void dispose(boolean close) {
        if (disposed) {
            return;
        }
        disposed = true;
        HandlerList.unregisterAll(this);
        if(close) {
            ((Player) inv.getHolder()).closeInventory();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (!e.getInventory().equals(inv)) {
            return;
        }
        dispose();
        if(Main.config.getBoolean("enable-player-warps")) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, () -> new MainMenu((Player) inv.getHolder()), 1L);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(!e.getInventory().equals(inv)) {
            return;
        }
        e.setCancelled(true);
        if(e.getRawSlot() == slotInfo) {
            return;
        } else if(e.getRawSlot() == slotLoc) {
            dispose(true);
            ChatConfigurator.newEditor(player, Property.LOCATION, warp);
        } else if(e.getRawSlot() == slotName) {
            dispose(true);
            ChatConfigurator.newEditor(player, Property.NAME, warp);
        } else if(e.getRawSlot() == slotDesc) {
            dispose(true);
            ChatConfigurator.newEditor(player, Property.DESCRIPTION, warp);
        } else if(e.getRawSlot() == slotIcon) {
            dispose(true);
            ChatConfigurator.newEditor(player, Property.ICON, warp);
        } else if(e.getRawSlot() == slotSort) {
            if(isAdmin) {
                dispose(true);
                ChatConfigurator.newEditor(player, Property.SORT_ID, warp);
            } else {
                double price = Main.config.getDouble("better-sort-price");
                if (!Main.economy.has(player, price)) {
                    player.sendMessage(ConfigManager.getString("menu.no-funds-sort"));
                    return;
                }
                Main.economy.withdrawPlayer(player, price);
                warp.setSort_id(warp.getSort_id() + 1);
                warp.updateInformationStorage();
                new MenuWarpEdit(player, warp, isAdmin);
            }
        } else if(e.getRawSlot() == slotDelete) {
            WarpManager.getWarpManager().deleteWarp(warp);
            player.sendMessage(ConfigManager.getString("setup.deleted"));
            dispose(true);
        }
    }

}
