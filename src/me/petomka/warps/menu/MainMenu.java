package me.petomka.warps.menu;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class MainMenu implements Menu, Listener {

    private Inventory inv;

    private int serverWarpSlot = 15, playerWarpSlot = 11, editPlayerWarpSlot = 31;

    MainMenu(Player player) {
        player.closeInventory();
        if (!Main.config.getBoolean("enable-player-warps")) {
            new MenuServerWarps(player);
            return;
        }
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        createInventory(player);
        player.openInventory(inv);
    }

    private void createInventory(Player player) {
        inv = Bukkit.createInventory(player, 9 * 5, ConfigManager.getStringNP("menu.title.main-menu"));

        ItemStack black = new ItemStack(Material.STAINED_GLASS_PANE);
        black.setDurability((short)15);
        ItemMeta bM = black.getItemMeta();
        bM.setDisplayName("§r");
        black.setItemMeta(bM);

        ItemStack sWarps = new ItemStack(Material.COMPASS);
        ItemMeta sWM = sWarps.getItemMeta();
        sWM.setDisplayName(ConfigManager.getStringNP("menu.option.server-warps"));
        sWarps.setItemMeta(sWM);

        ItemStack pWarps;
        ItemStack editPWarps;

        pWarps = new ItemStack(Material.SKULL_ITEM);
        pWarps.setDurability((short) SkullType.PLAYER.ordinal());
        ItemMeta pWM = pWarps.getItemMeta();
        pWM.setDisplayName(ConfigManager.getStringNP("menu.option.player-warps"));
        pWarps.setItemMeta(pWM);

        editPWarps = new ItemStack(Material.EMPTY_MAP);
        ItemMeta ePWM = editPWarps.getItemMeta();
        ePWM.setDisplayName(ConfigManager.getStringNP("menu.option.edit-own-warps"));
        editPWarps.setItemMeta(ePWM);

        for(int index = 0; index < 9*5; index++) {
            inv.setItem(index, black);
        }

        inv.setItem(serverWarpSlot, sWarps);
        inv.setItem(playerWarpSlot, pWarps);
        inv.setItem(editPlayerWarpSlot, editPWarps);

    }

    private boolean disposed = false;

    private void dispose() {
        if(disposed) {
            return;
        }
        disposed = true;
        HandlerList.unregisterAll(this);
        ((Player)inv.getHolder()).closeInventory();
        inv = null;
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        if(e.getPlayer().equals(inv.getHolder())) {
            dispose();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if(e.getInventory().equals(inv)) {
            dispose();
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(!(e.getWhoClicked() instanceof Player)) {
            return;
        }
        if (!e.getInventory().equals(inv)) {
            return;
        }
        if(e.getRawSlot() == serverWarpSlot) {
            dispose();
            new MenuServerWarps((Player) e.getWhoClicked());
        } else if(e.getRawSlot() == playerWarpSlot) {
            dispose();
            new MenuPlayerWarps((Player) e.getWhoClicked());
        } else if(e.getRawSlot() == editPlayerWarpSlot) {
            dispose();
            new MenuOwnWarps((Player)e.getWhoClicked());

        }
        e.setCancelled(true);
    }

}
