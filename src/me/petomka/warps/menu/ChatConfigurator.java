package me.petomka.warps.menu;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.warp.Warp;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class ChatConfigurator implements Listener {

    private static HashMap<Player, ChatConfigurator> configuring = new HashMap<>();

    public static boolean isEditing(Player player) {
        return configuring.containsKey(player);
    }

    public static void newEditor(Player player, Property property, Warp toEdit) {
        if(isEditing(player)) {
            return;
        }
        configuring.put(player, new ChatConfigurator(player, property, toEdit));
    }

    private Player player;
    private Property property;
    private Warp toEdit;

    private ChatConfigurator(Player player, Property property, Warp toEdit) {
        this.player = player;
        this.property = property;
        this.toEdit = toEdit;
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        switch (property) {
            case NAME:
            case DESCRIPTION: {
                player.sendMessage(ConfigManager.getString("config.enter-string").replaceAll("<property>",
                        ConfigManager.getStringNP("config.property." + (property == Property.NAME ? "name" : "description"))));
                break;
            }
            case SORT_ID: {
                player.sendMessage(ConfigManager.getString("config.enter-int"));
                break;
            }
            case LOCATION: {
                player.sendMessage(ConfigManager.getString("config.sneak-position"));
                break;
            }
            case ICON: {
                player.sendMessage(ConfigManager.getString("config.click-item"));
                break;
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if(event.getPlayer().equals(player)) {
            doneEditing();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event) {
        if(!event.getPlayer().equals(player)) {
            return;
        }
        event.setCancelled(true);
        if(event.getMessage().equalsIgnoreCase(ConfigManager.getStringNP("config.quit-string"))) {
            doneEditing();
            return;
        }
        switch (property) {
            case DESCRIPTION: {
                toEdit.setDescription(event.getMessage());
                break;
            }
            case NAME: {
                toEdit.setName(event.getMessage());
                break;
            }
            case SORT_ID: {
                Pattern number = Pattern.compile("-?[0-9]+");
                if(!number.matcher(event.getMessage()).matches()) {
                    event.getPlayer().sendMessage(ConfigManager.getString("config.enter-int"));
                    return;
                }
                toEdit.setSort_id(Integer.parseInt(event.getMessage()));
                break;
            }
            default: {
                player.sendMessage(ConfigManager.getString("config.quit-info").replaceAll("<quit>", ConfigManager.getStringNP("config.quit-string")));
                return;
            }
        }
        toEdit.updateInformationStorage();
        player.sendMessage(ConfigManager.getString("config.success"));
        player.sendMessage(ConfigManager.getString("config.quit-info").replaceAll("<quit>", ConfigManager.getStringNP("config.quit-string")));
    }

    @EventHandler
    public void onSneak(PlayerToggleSneakEvent event) {
        if(!event.getPlayer().equals(player)) {
            return;
        }
        if(!event.isSneaking()) {
            return;
        }
        if(property != Property.LOCATION) {
            return;
        }
        toEdit.setLocation(event.getPlayer().getLocation());
        toEdit.updateInformationStorage();
        player.sendMessage(ConfigManager.getString("config.success"));
        player.sendMessage(ConfigManager.getString("config.quit-info").replaceAll("<quit>", ConfigManager.getStringNP("config.quit-string")));
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent event) {
        if(!event.getPlayer().equals(player)) {
            return;
        }
        if(!(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            return;
        }
        if(property != Property.ICON) {
            return;
        }
        event.setCancelled(true);
        ItemStack is = event.getItem();
        if(is == null) {
            return;
        }
        if(is.getType() == Material.AIR) {
            return;
        }
        toEdit.setIcon(is);
        toEdit.updateInformationStorage();
        player.sendMessage(ConfigManager.getString("config.success"));
        player.sendMessage(ConfigManager.getString("config.quit-info").replaceAll("<quit>", ConfigManager.getStringNP("config.quit-string")));
    }

    private void doneEditing() {
        if(player.isOnline()) {
            player.sendMessage(ConfigManager.getString("config.quit-config"));
        }
        configuring.remove(player);
        Bukkit.getPluginManager().callEvent(new ConfigureFinishEvent(player));
        HandlerList.unregisterAll(this);
    }

}
