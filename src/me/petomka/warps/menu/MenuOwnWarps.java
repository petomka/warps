package me.petomka.warps.menu;

import me.petomka.warps.Main;
import me.petomka.warps.config.ConfigManager;
import me.petomka.warps.teleports.TeleportTask;
import me.petomka.warps.warp.Warp;
import me.petomka.warps.warp.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benedikt on 29.07.2017.
 */
public class MenuOwnWarps implements Menu, Listener {

    private Player player;
    private Inventory inv;
    private int mainMenu = 42, pageDown = 43, pageUp = 44, createOwnWarp = 40, page = 0;

    private List<Warp> playersWarps;

    public MenuOwnWarps(Player player) {
        player.closeInventory();
        this.player = player;
        createInventory(player);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        player.openInventory(inv);
    }

    private void createInventory(Player player) {
        this.inv = Bukkit.createInventory(player, 5 * 9, ConfigManager.getStringNP("menu.title.own-warps"));

        ItemStack black = new ItemStack(Material.STAINED_GLASS_PANE);
        black.setDurability((short) 15);
        ItemMeta meta = black.getItemMeta();
        meta.setDisplayName("§r");
        black.setItemMeta(meta);

        ItemStack blue = new ItemStack(Material.STAINED_GLASS_PANE);
        blue.setDurability((short) 11);
        blue.setItemMeta(meta);

        playersWarps = WarpManager.getWarpManager().getPlayersWarps(player);

        for (int index = 0; index < 9 * 5; index++) {
            if (index > 35) {
                inv.setItem(index, blue);
            } else {
                if (index + ((page * 36)) < playersWarps.size()) {
                    ItemStack icon = playersWarps.get(index + (page * 36)).getIcon();
                    ItemMeta iconMeta = icon.getItemMeta();
                    List<String> lorelist = iconMeta.getLore();
                    lorelist.add(ConfigManager.getStringNP("menu.option.right-click-edit"));
                    iconMeta.setLore(lorelist);
                    icon.setItemMeta(iconMeta);
                    inv.setItem(index, icon);
                } else {
                    inv.setItem(index, black);
                }
            }
        }

        ItemStack createWarp = new ItemStack(Material.ANVIL);
        ItemMeta createWarpMeta = createWarp.getItemMeta();
        String[] nameAndLore = ConfigManager.getStringList("menu.option.add-own-warp").toArray(new String[0]);
        if(nameAndLore.length == 0) {
            throw new IllegalArgumentException("Setup your config properly! Value for menu.option.add-own-warp cannot be null!");
        }
        double price = Main.config.getDouble("warp-price");
        if(Main.config.getBoolean("grow-price")) {
            price = price * (playersWarps.size()+1);
        }
        createWarpMeta.setDisplayName(nameAndLore[0].replaceAll("<price>", String.valueOf(price)));
        List<String> lore = new ArrayList<>();
        for(int a = 1; a < nameAndLore.length; a++) {
            lore.add(nameAndLore[a].replaceAll("<price>", String.valueOf(price)));
        }
        createWarpMeta.setLore(lore);
        createWarp.setItemMeta(createWarpMeta);

        inv.setItem(createOwnWarp, createWarp);

        ItemStack[] ps = MenuManager.getPagesSelectors();

        inv.setItem(mainMenu, ps[0]);
        inv.setItem(pageDown, ps[1]);
        inv.setItem(pageUp, ps[2]);
    }

    private boolean disposed = false;

    private void dispose() {
        dispose(false);
    }

    private void dispose(boolean close) {
        if (disposed) {
            return;
        }
        disposed = true;
        HandlerList.unregisterAll(this);
        if(close) {
            ((Player) inv.getHolder()).closeInventory();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (!e.getInventory().equals(inv)) {
            return;
        }
        dispose();
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, () -> new MainMenu((Player) inv.getHolder()), 1L);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (!e.getInventory().equals(inv)) {
            return;
        }

        e.setCancelled(true);

        if (e.getClick() == ClickType.LEFT) {
            if (e.getRawSlot() < 36) {
                if (e.getRawSlot() + page * 36 >= playersWarps.size()) {
                    return;
                }
                int index = e.getRawSlot() + page * 36;
                if (index < 0) {
                    return;
                }
                dispose(true);
                TeleportTask.queueTeleport((Player) inv.getHolder(), playersWarps.get(index).getLocation());
            }

            if (e.getRawSlot() == mainMenu) {
                dispose(true);
                new MainMenu((Player) inv.getHolder());
            } else if (e.getRawSlot() == pageDown) {
                if (page < 1) {
                    ((Player) inv.getHolder()).sendMessage(ConfigManager.getString("menu.no-page"));
                    return;
                }
                page -= 1;
                createInventory((Player) inv.getHolder());
            } else if (e.getRawSlot() == pageUp) {
                if (!((page + 1) * 9 * 4 < playersWarps.size())) {
                    ((Player) inv.getHolder()).sendMessage(ConfigManager.getString("menu.no-page"));
                    return;
                }
                page += 1;
                createInventory((Player) inv.getHolder());
            } else if (e.getRawSlot() == createOwnWarp) {
                double price = Main.config.getDouble("warp-price");
                if(Main.config.getBoolean("grow-price")) {
                    price = price * (playersWarps.size()+1);
                }
                if (!Main.economy.has(player, price)) {
                    player.sendMessage(ConfigManager.getString("menu.no-funds-warp"));
                    return;
                }
                Main.economy.withdrawPlayer(player, price);
                dispose(true);
                new ChatSetup((Player) e.getWhoClicked(), true);
            }
        } else if (e.getClick() == ClickType.RIGHT) {
            int index = e.getRawSlot() + page * 36;
            dispose(true);
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.main, () -> new MenuWarpEdit((Player)e.getWhoClicked(), playersWarps.get(index), player.hasPermission("warps.admin")), 1L);
        }

    }

}
