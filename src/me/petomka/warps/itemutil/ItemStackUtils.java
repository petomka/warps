package me.petomka.warps.itemutil;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

/**
 * Created by Benedikt on 24.07.2017.
 */
public final class ItemStackUtils {

    public static String getEnchants(ItemStack i){
//        List<String> e = new ArrayList<>();
//        Map<Enchantment, Integer> en = i.getEnchantments();
//        for(Enchantment t : en.keySet()) {
//            e.add(t.getName() + ":" + en.get(t));
//        }
        return /*StringUtils.join(e, ",")*/ i.getItemMeta().hasEnchants() ? "GLOWING" : "";
    }

    @SuppressWarnings("deprecation")
    public static String serialize(ItemStack i){
        String[] parts = new String[6];
        parts[0] = i.getType().name();
        parts[1] = Integer.toString(i.getAmount());
        parts[2] = String.valueOf(i.getDurability());
        parts[3] = i.getItemMeta().getDisplayName();
        parts[4] = String.valueOf(i.getData().getData());
        parts[5] = getEnchants(i);
        return Base64Coder.encodeString(StringUtils.join(parts, ";"));
    }

    @SuppressWarnings("deprecation")
    public static ItemStack deserialize(String p){
        p = Base64Coder.decodeString(p);
        String[] a = p.split(";");
        ItemStack i = new ItemStack(Material.getMaterial(a[0]), Integer.parseInt(a[1]));
        i.setDurability((short) Integer.parseInt(a[2]));
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(a[3]);
        i.setItemMeta(meta);
        MaterialData data = i.getData();
        data.setData((byte) Integer.parseInt(a[4]));
        i.setData(data);
        if (a.length > 5) {
//            String[] parts = a[5].split(",");
//            for (String s : parts) {
//                String label = s.split(":")[0];
//                String amplifier = s.split(":")[1];
//                Enchantment type = Enchantment.getByName(label);
//                if (type == null)
//                    continue;
//                int f;
//                try {
//                    f = Integer.parseInt(amplifier);
//                } catch(Exception ex) {
//                    continue;
//                }
//
//                meta.addEnchant(type, f, true);
//            }
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        i.setItemMeta(meta);
        return i;
    }

}
