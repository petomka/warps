package me.petomka.warps.config;

import me.petomka.warps.Main;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benedikt on 23.07.2017.
 */
public class ConfigManager {

    public static String getString(String path) {
        return ConfigManager.getPrefix() + ConfigManager.getStringNP(path);
    }

    public static String getStringNP(String path) {
        return Main.config.getString(path).replaceAll("&", "§").replaceAll("§§", "&").replaceAll("<symbol>", Main.economy.currencyNamePlural());
    }

    public static List<String> getStringList(String path) {
        List<String> origin = Main.config.getStringList(path);
        List<String> result = new ArrayList<>();
        for(String s : origin) {
            result.add(s.replaceAll("&", "§").replaceAll("§§", "&").replaceAll("<symbol>", Main.economy.currencyNamePlural()));
        }
        return result;
    }

    public static String getPrefix() {
        return Main.config.getString("prefix").replaceAll("&", "§").replaceAll("§§", "&");
    }

}
