package me.petomka.warps.mysql;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class MySQL {

	private static String host;
	private static int port;
	private static String user;
	private static String password;
	private static String database;

	private static Connection conn;

	public static void initMySQL() throws Exception {
		File file = new File("plugins/Warps/", "database.yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);

		String db = "database.";

		config.addDefault(db + "host", "hostname");
		config.addDefault(db + "port", 3306);
		config.addDefault(db + "user", "username");
		config.addDefault(db + "password", "password");
		config.addDefault(db + "database", "name");
		config.options().copyDefaults(true);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		host = config.getString(db + "host");
		port = config.getInt(db + "port");
		user = config.getString(db + "user");
		password = config.getString(db + "password");
		database = config.getString(db + "database");
		openConnection();
		//Bukkit.getScheduler().runTaskLaterAsynchronously(TopiCore.getPlugin(), new MySQLAntiCrash(), 30*20L);
	}
	
	public static String getDatabase() {
		return database;
	}

	/**
	 * Versucht eine Verbindung mit dem in der mysql.yml angegebenen Server aufzubauen.
	 * @return die aufgebaute Verbindung
	 * @throws Exception Fehler beim Verbindungsaufbau
	 */
	public static Connection openConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true&useSSL=false", user, password);
		return conn;
	}

	/**
	 * Gibt die Verbindung
	 * @return Verbindung
	 */
	public static Connection getConnection() {
		return conn;
	}

	/**
	 * Testet ob eine gültige Verbindung besteht
	 * @return true wenn ja
	 */
	public static boolean hasConnection() {
		try {
			return conn != null && conn.isValid(1);
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Sendet ein Update an die Datenbank mit einem PreparedStatement
	 * @param st die Abfrage
	 * @throws Exception Fehler bei der Abfrage oder bei der Verbindung
	 */
	public static void executeUpdate(PreparedStatement st) throws Exception {
		reconnect();
		st.executeUpdate();
		closeRessources(null, st);
	}

	/**
	 * Sendet einen direkten Updatebefehl an die Datenbank
	 * @param query Was die Datenbank tun soll
	 * @throws Exception Fehler in der Abfrage oder in der Verbindung
	 */
	public static void executeUpdate(String query) throws Exception {
		reconnect();
		PreparedStatement st = conn.prepareStatement(query);
		st.executeUpdate();
		closeRessources(null, st);
	}

	/**
	 * Gibt die erste mit dieser Abfrage gefundenen Tupel aus
	 * @param query Nach was abefragt werden soll
	 * @param key Welche Spalte du aus dem Ergebnis willst
	 * @return Die angegebene Spalte, null wenn keine gefunden
	 * @throws Exception Fehler in der Abfrage oder in der Verbindung
	 */
	public static Object executeQuery(String query, String key) throws Exception {
		reconnect();
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery(query);
		Object o = null;
		if(rs.next()) {
			o = rs.getObject(key);
		}
		rs.close();
		stmt.close();
		return o;
	}
	
	/**
	 * Gibt Spalte aus Datenbank aus
	 * @param query SQL-Query
	 * @param key Spalte, die die Array-List speichern soll
	 * @return die ArrayList mit allen Werten der Spalte, ist nie null
	 * @throws Exception SQL-Fehler
	 */
	public static ArrayList<Object> executeQueries(String query, String key) throws Exception {
		reconnect();
		ArrayList<Object> results = new ArrayList<Object>();
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			results.add(rs.getObject(key));
		}
		return results;
	}

	/**
	 * Alle Ergebnisse die deine Anfrage bringt, werden in der zurückgegebenen HashMap gespeichert.
	 * Die Key-Value ist der Name der Spalte der abgefragten Tabelle, die zurückgegebene ArrayList ist die
	 * gesamte Spalte.
	 * Gleiche Indezes bei verschiedenen ArrayLists aus dieser HashMap bilden also einen Datensatz.
	 *
	 * @param query Die Abfrage
	 * @return die resultierende Tabelle
	 * @throws Exception Fehler in der Abfrage oder in der Verbindung
	 */
	public static HashMap<String, ArrayList<Object>> executeManyQueries(String query) throws Exception {
		reconnect();
		HashMap<String, ArrayList<Object>> columns = new HashMap<String, ArrayList<Object>>();
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();
		ResultSetMetaData rd = rs.getMetaData();
		ArrayList<ArrayList<Object>> lists = new ArrayList<ArrayList<Object>>();
		for(int i = 0; i < rd.getColumnCount(); i++) {
			lists.add(new ArrayList<Object>());
		}
		while(rs.next()) {
			for(int i = 0; i < rd.getColumnCount(); i++) {
				lists.get(i).add(rs.getObject(i+1));
			}
		}
		for(int i = 0; i < rd.getColumnCount(); i++) {
			columns.put(rd.getColumnLabel(i+1), lists.get(i));
		}
		return columns;
	}

	/**
	 * Bereitet ein Statement auf die Abfrage vor.
	 * Ein '?' wird durch das jeweilige Argument-Objekt ersetzt.
	 *
	 * Bsp:
	 * int a = 15;
	 * PreparedStatement ps = registerParameters("SELECT * FROM tabelle WHERE id = ?", a);
	 *
	 * @param query Die Abfrage
	 * @param args Die Objekte die im PreparedStatement ersetzt werden sollen
	 * @return das PreparedStatement
	 * @throws Exception Fehler in der Abfrage oder in der Verbindung
	 */
	public static PreparedStatement registerParameters(final String query, Object... args) throws Exception {
		reconnect();
        PreparedStatement stmt = conn.prepareStatement(query);
        int i = 1;
        for (Object arg : args) {         
            if (arg instanceof Date) {
                stmt.setTimestamp(i++, new Timestamp(((Date) arg).getTime()));
            } else if (arg instanceof Integer) {
                stmt.setInt(i++, (Integer) arg);
            } else if (arg instanceof Long) {
                stmt.setLong(i++, (Long) arg);
            } else if (arg instanceof Double) {
                stmt.setDouble(i++, (Double) arg);
            } else if (arg instanceof Float) {
                stmt.setFloat(i++, (Float) arg);
            } else {
                stmt.setString(i++, (String) arg);
            }
        }  
        return stmt;
    }

	/**
	 * Frägt die Datenbank mit dem übergebenen PreparedStatement ab.
	 * @param stmt Das Statement
	 * @param lookup Aus welcher Spalte du das Ergebnis willst
	 * @return Das Ergebnis aus gegebener Spalte
	 * @throws Exception Fehler in der Abfrage oder in der Verbindung
	 */
	public static Object executeAndGet(PreparedStatement stmt, String lookup) throws Exception{
		reconnect();
        ResultSet rs = stmt.executeQuery();
        Object o = null;
        if(rs.next()) {
            o = rs.getObject(lookup);
        }
        rs.close();
        stmt.close();
        return o;
    }

	/**
	 * Updatet die Datenbank mit dem übergebenen PreparedStatement.
	 * @param stmt das Statement
	 * @throws Exception Fehler in der Abfrage oder in der Verbindung
	 */
	public static void executeAndUpdate(PreparedStatement stmt) throws Exception {
		reconnect();
        stmt.executeUpdate();
        stmt.close();
    }

	private static void closeRessources(ResultSet rs, PreparedStatement st) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Schliesst die Datenbankverbindung.
	 */
	public static void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			conn = null;
		}
	}

	/**
	 * Versucht die Datenkbankverbindung erneut herzustellen wenn nicht mehr vorhanden
	 * @throws Exception Fehler in der Verbindung
	 */
	public static void reconnect() throws Exception {
		if(hasConnection()) return;
		closeConnection();
		openConnection();
	}
}

